import React, { useState, useEffect } from "react";
import { PageHeader, ListGroup } from "react-bootstrap";

import "./Common.css";
import { Auth, API } from "aws-amplify";

export default function Quotes(props) {
  const [quotes, setQuotes] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    async function onLoad() {
      // if (!props.isAuthenticated) {
      //   return;
      // }

      try {
        const quotes = await loadQuotes();
        setQuotes(quotes);
        console.log(quotes);
      } catch (e) {
        console.error(e);
      }

      setIsLoading(false);
    }

    onLoad();
  }, [props.isAuthenticated]);

  async function loadQuotes() {
    // const user = await Auth.currentAuthenticatedUser();
    // const token = user.signInUserSession.idToken.jwtToken    
    return API.get("quotes", "/quotes");
  }

  return (
    <div className="Home">
      <div className="lander">
        <h1>Quotes</h1>
        <p>Existing Quotes page</p>
      </div>
    </div>
  );
}