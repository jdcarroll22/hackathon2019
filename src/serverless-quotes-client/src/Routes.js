import React, { useState } from "react";
import { Auth } from 'aws-amplify';

import { Route, Switch } from "react-router-dom";
import Home from "./containers/Home";
import Quotes from "./containers/Quotes";
import Vehicles from "./containers/Vehicles";
import Dealers from "./containers/Dealers";
import Signup from "./containers/Signup";
import Login from "./containers/Login";
import NotFound from "./containers/NotFound";

export default function Routes() {
  const [isAuthenticated, userHasAuthenticated] = useState(true);

  // Temporarily - Manual signing
  var [email, password] = ["admin@example.com", "Passw0rd!"];
  Auth.signIn(email, password);

  return (
    <Switch>
      <Route path="/" exact component={Home} props={isAuthenticated} />
      <Route path="/quotes" exact component={Quotes} props={isAuthenticated} />
      <Route path="/vehicles" exact component={Vehicles} props={isAuthenticated} />
      <Route path="/dealers" exact component={Dealers} props={isAuthenticated} />
      <Route path="/signup" exact component={Signup} />
      <Route path="/login" exact component={Login} />
      
      { /* Finally, catch all unmatched routes */ }
      <Route component={NotFound} />
    </Switch>
  );
}