export default {
    s3: {
      REGION: "us-east-1",
      BUCKET: "serverless-quotes-staticsite"
    },
    apiGateway: {
      REGION: "us-east-1",
      URL: "https://f1up6edkri.execute-api.us-east-1.amazonaws.com/prod"
    },
    cognito: {
      REGION: "us-east-1",
      USER_POOL_ID: "us-east-1_mVvWedbiA",
      APP_CLIENT_ID: "6qo3glrvja76qo6uofu3uqg9lg",
      IDENTITY_POOL_ID: "us-east-1:1d2b327c-2294-4d36-be71-afd89994b371"
    }
  };
