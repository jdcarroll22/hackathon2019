import * as dynamoDbLib from "../lib/dynamodb-lib";
import { success, failure } from "../lib/response-lib";

export async function main(event, context) {
    const params = {
        TableName: process.env.vehicles_tablename,
        // 'KeyConditionExpression' defines the condition for the query
        // - 'userId = :userId': only return items with matching 'userId'
        //   partition key
        // 'ExpressionAttributeValues' defines the value in the condition
        // - ':userId': defines 'userId' to be Identity Pool identity id
        //   of the authenticated user
        KeyConditionExpression: "userId = :userId",
        ExpressionAttributeValues: {
            ":userId": event.requestContext.identity.cognitoIdentityId
        }
    };

    try {
        const result = await dynamoDbLib.call("query", params);
        return success(result);
    } catch (e) {
        return failure({ status: false, error: e, TableName: process.env.quotes_tablename, event, context });
    }
}